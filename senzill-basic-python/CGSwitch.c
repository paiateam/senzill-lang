/*************************************************************************** 
Code generator switch
Authors: Joel Cemeli
         Eduard Torres
***************************************************************************/
#include "SM.h"
#include "CGSwitch.h"
#include "CG.h"
#include "PYCG.h"

int sz_compilation = 1;

void compile_to_py() {
    sz_compilation = 0;
    data_location = py_data_location;
    gen_code = py_gen_code;
    gen_name = py_gen_name;
    reserve_loc = py_reserve_loc;
    back_patch = py_back_patch;
    gen_label = py_gen_label;
    print_code = py_print_code;
    read_bytecode = py_read_bytecode;
    write_bytecode = py_write_bytecode;
}

void compile_to_sz() {
    sz_compilation = 1;
    data_location = sz_data_location;
    gen_code = sz_gen_code;
    gen_name = sz_gen_name;
    reserve_loc = sz_reserve_loc;
    back_patch = sz_back_patch;
    gen_label = sz_gen_label;
    print_code = sz_print_code;
    read_bytecode = sz_read_bytecode;
    write_bytecode = sz_write_bytecode;
}

int is_sz_compilation() {
    return sz_compilation;
}