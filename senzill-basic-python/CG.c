/*************************************************************************** 
Code Generator 
Author: Anthony A. Aaby
Modified by: Jordi Planes
Extended by: Joel Cemeli
             Eduard Torres
***************************************************************************/ 

#include <stdio.h>
#include "SM.h"
#include "CG.h"

/*------------------------------------------------------------------------- 
Data Segment 
-------------------------------------------------------------------------*/ 
int data_offset = 0; /* Initial offset */ 
int sz_data_location() /* Reserves a data location */ 
{ 
  return data_offset++; 
} 

/*------------------------------------------------------------------------- 
  Code Segment 
  -------------------------------------------------------------------------*/ 
int sz_code_offset = 0;
int sz_gen_label() /* Returns current offset */ 
{ 
  return sz_code_offset; 
} 

int sz_reserve_loc() /* Reserves a code location */ 
{ 
  return sz_code_offset++; 
} 

/* Generates code at current location */ 
void sz_gen_code( enum code_ops operation, int arg ) 
{ 
  code[sz_code_offset].op = operation; 
  code[sz_code_offset++].arg = arg; 
} 

/* Generates code at a reserved location */ 
void sz_back_patch( int addr, enum code_ops operation, int arg ) 
{ 
  code[addr].op = operation; 
  code[addr].arg = arg; 
}

/* Only used to keep CGSwitch interface */
void sz_gen_name(char * name) {

}

/*------------------------------------------------------------------------- 
Print Code to stdio 
-------------------------------------------------------------------------*/ 
void sz_print_code() 
{ 
  int i = 0; 
  while (i < sz_code_offset) { 
    printf("%3d: %-10s%4d\n",i,op_name[(int) code[i].op], code[i].arg ); 
    i++; 
  } 
} 

/* Reads code from file */

void sz_read_bytecode( char *file_name ) {
  FILE * bytecode_file = fopen( file_name, "r" );
  int file_code[2];
  int i;  

  fread( &sz_code_offset, sizeof( sz_code_offset ), 1, bytecode_file  );
  fread( &data_offset, sizeof( data_offset ), 1, bytecode_file  );
#ifndef NDEBUG
  printf("Offsets: %d %d\n", sz_code_offset, data_offset );
#endif
  for( i = 0; i < MAX_MEMORY && fread( file_code, sizeof( file_code ), 1, bytecode_file  ) != 0; i++ ) {
    sz_back_patch( i, file_code[0], file_code[1] );
  }
  fclose( bytecode_file );
}

/* Writes code to file */

void sz_write_bytecode( char *file_name ) {
  FILE * bytecode_file = fopen( file_name, "w" );
  int file_code[2];
  int i;
  
  fwrite( &sz_code_offset, sizeof( sz_code_offset ), 1, bytecode_file  );
  fwrite( &data_offset, sizeof( data_offset ), 1, bytecode_file  );
  for( i = 0; i < MAX_MEMORY; i++ ) {
    file_code[0] = code[ i ].op;
    file_code[1] = code[ i ].arg;
    fwrite( file_code, sizeof( file_code ), 1, bytecode_file  );
  }
  fclose( bytecode_file );
}

/************************** End Code Generator **************************/ 
