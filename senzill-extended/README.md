# SENZILL EXTENDED#
 
Extended version of *senzill* provides the following extensions for the original language:
    * Modification of grammar
    * Functions (declaration, implementation and invocation)
    * Static arrays
    * Error control and basic (and colourful) logging system
    * Comments treating
    * Inclusion of functions declared and implemented in other files
    * Include guards
 
##*Compilation*##
 
make
 
##*Compiling a file*##
 
./senzill myprogram.sz myOutput.szc  [(-v | --verbose) (info|warn|error) ] 
 
##Symbol table extension##
 
We added new structure storing as function and arrays that can be stored on the symbol table on the “symrecs”, that can be requested by their type (function, array, etc…). Also we added more control into variables/functions, in order to know if they are declared yet or not declared and throwing the proper error through the logger.
 
##Modification of grammar##
 
The new grammar will offer the following syntax: *init* [functions & includes] *let*[variables declaration for main] *int* [main code] *end*
 
*Init* order exists in order to create two data locations to provide “swap” to array, also a “goto” that will be backpatched with the proper address in order to jump directly into reserve full amount of data needed and then main code.
 
Also all the “synchonization” tokens are ‘;’ instead of mixing 
 
##Functions##
 
Now *senzill* provides functions, that need to be declared inside “init” section, their syntax is the following:
 
```
integer functionName( a b c)
do
instructions and declarations (can be mixed);
return a;
end
```
 
they are stored on the ST, with all the necessary information in order to parse the proper code (above the main), that will be called from it (but not between functions).
 
Function usage from main:
 
```
/*Can be called with entire numbers, expressions and variables, functions are also considerated as an expresions*/
a = functionName(1 3*4 b) 
```
 
##Static arrays##
 
The arrays are also stored on the improved ST with their size and start position. on memory. They can be used on the following way:
 
```
integer myArray[25]; //For declaration
 
write myArray[2]; // To access a position from the array (and print it in this case)
 
read a;
write myArray[a]; //To access a variable position from the array (and print it in this case)
 
myArray[a] = 2; //To store data on a variable position from the array
 
write length myArray; //To access to the size of the array (and print it in this case)
```
The “swap” memory reserved space on init is used in order to acces to the array positions by an “expresion”, to perform the store_sub the necessary values was stored inverse on the stack (the assign value on the top and the dynamic position below that), so we use this swap space in order to invert it (using assembler instructions).
 
##Error control and colorful logging system##
 
*NOTE:* For all errors we control the line where was it found and also the file (for the includes) and it will be printed always.
 
We control the the input treatment at different levels (Errors, warnings and info)
 
###Errors###
 
 *Program structure.
 *Function and variables existance (Not declared / Declared yet)
 *Program syntax.
*Error reading files
*Exceeding max include depth.
 
###Warnings###
 * Include guards (File already included)
 
###Info###
 
For every events like function reading (also adding its params and variables).
 
The logging system prints all the messages (depending on the selected log level) with a different color for each type of printed message and also a final count of each type.
 
##Comments treating##
All the comments with the following syntax will be ignored:
```
//Line comment
 
/*This is
a block
comment*/
```
##Inclusion of functions declared and implemented in other files##
 
Files will be included by using *#include* instruction. The files on the include will be looked up with the specified path (taking from point of start the file where the “main” file is placed)
 
The files that are included only can contain a declared functions and other #includes.
 
##Include guards##
 
We also provide include guards, the program will take care about if a file it’s already included (if it’s already included it will warn to the user and not include it twice).
 
 
 
 
 
 
 
 
