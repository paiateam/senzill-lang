/*************************************************************************** 
  Python Code Generator
***************************************************************************/ 

#ifndef __PYCG_H
#define __PYCG_H

#include <stdio.h>
#include "SM.h"

#define MAX_MEMORY 999

extern int py_code_offset; 

/* Python Bytecode instructions */
enum PyCodes {
  STORE_NAME = 90,
  LOAD_CONST = 100,
  PRINT_ITEM = 71,
  PRINT_NEWLINE = 72,
  LOAD_NAME = 101,
  RETURN_VALUE = 83,
  BINARY_ADD = 23,
  BINARY_SUBTRACT = 24,
  BINARY_MULTIPLY = 20,
  BINARY_DIVIDE = 21,
  CALL_FUNCTION = 131,
  COMPARE_OP = 107,
  POP_JUMP_IF_FALSE = 114,
  JUMP_ABSOLUTE = 113
};

enum PyComparationCodes {
  LESS = 0,
  EQUALS = 2,
  GREATER = 4
};

/* Python instruction */
struct PyInstruction 
{ 
  enum PyCodes op; 
  int arg;
  int bytePos;
};

/* Python Name */
struct PyName
{
  char * name;
  int offset;
};

/* Python Constant */
struct PyConst
{
  int value;
  int offset;
};

/* Array of python instructions */
struct PyInstruction py_code[MAX_MEMORY];
/* Array of python names */
struct PyName py_names[MAX_MEMORY];
/* Array of python constants */
struct PyConst py_constants[MAX_MEMORY];


int  py_data_location();
void py_gen_code( enum code_ops operation, int arg );
void py_gen_name(char * name);
int  py_reserve_loc();
void py_back_patch( int addr, enum code_ops operation, int arg );
int  py_gen_label();
void py_print_code();
void py_read_bytecode( char *file_name );
void py_write_bytecode( char *file_name );

/* PRIVATE FUNCTIONS */

/*------------------------------------------------------------------------- 
Translation of Senzill instructions to Python Bytecode instructions
-------------------------------------------------------------------------*/ 
void _gen_py_code(enum PyCodes code, int arg);
void _store(int arg);
void _ld_int(int arg);
void _write_int(int arg);
void _ld_var(int arg);
void _halt(int arg);
void _addOp(enum code_ops operation);
void _readInt(int arg);
void _callFunction(int arg);
void _compareOp(enum code_ops code);
void _jumpFalse(int arg);
void _jumpAbsolute(int arg);

/*------------------------------------------------------------------------- 
Auxiliary functions of write_bytecode
-------------------------------------------------------------------------*/ 
void _write_header(FILE * ptr_file);
void _write_instructions(FILE * ptr_file);
void _write_constants(FILE * ptr_file);
void _write_names(FILE * ptr_file);
void _write_common_footer(FILE * ptr_file);
void _write_filename(FILE * ptr_file);
void _write_module(FILE * ptr_file);
void _write_end_footer(FILE * ptr_file);

#endif
