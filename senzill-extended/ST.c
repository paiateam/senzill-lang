/*************************************************************************** 
Symbol Table Module 
Author: Anthony A. Aaby
Modified by: Jordi Planes
Extended by: Joel Cemeli
             Eduard Torres
***************************************************************************/ 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CG.h"
#include "ST.h"

/*========================================================================= 
DECLARATIONS 
=========================================================================*/ 

/*------------------------------------------------------------------------- 
SYMBOL TABLE ENTRY 
-------------------------------------------------------------------------*/ 
symrec *identifier; 

/*------------------------------------------------------------------------- 
SYMBOL TABLE 
Implementation: a chain of records. 
------------------------------------------------------------------------*/ 
symrec *sym_table = (symrec *)0; /* The pointer to the Symbol Table */ 

/*======================================================================== 
  Operations: Getsym, Putincl, Putvar, Putfun, Putarr
  ========================================================================*/ 
symrec * getsym (char *sym_name, enum Type type) 
{ 
  symrec *ptr; 
  for (ptr = sym_table; ptr != (symrec *) 0; ptr = (symrec *)ptr->next) 
    if (strcmp (ptr->name,sym_name) == 0 && ptr->type == type) 
      return ptr; 
  return NULL;
}

symrec * putincl(char *incl_name) {
  symrec *ptr; 
  ptr = (symrec *) malloc(sizeof(symrec));
  ptr->name = (char *) malloc(strlen(incl_name)+1); 
  strcpy (ptr->name,incl_name);
  ptr->offset = 0; /* Offset does not matter */
  ptr->next = (struct symrec *)sym_table;
  ptr->type = Incl;
  sym_table = ptr; 
  return ptr; 
}

symrec * putvar(char *var_name) {
  symrec *ptr; 
  ptr = (symrec *) malloc(sizeof(symrec));
  ptr->name = (char *) malloc(strlen(var_name)+1); 
  strcpy (ptr->name,var_name);
  ptr->offset = data_location();
  ptr->next = (struct symrec *)sym_table;
  ptr->type = Var;
  sym_table = ptr; 
  return ptr; 
}

symrec * putfun(char *fun_name) {
  symrec *ptr; 
  ptr = (symrec *) malloc(sizeof(symrec));
  ptr->name = (char *) malloc(strlen(fun_name)+1); 
  strcpy (ptr->name,fun_name);
  ptr->offset = gen_label();
  ptr->function = (struct function*) malloc(sizeof(struct function));
  ptr->function->num_params = 0;
  ptr->function->num_vars = 0;
  ptr->function->ret_offset = data_location(); /* Allocate space for return variable */
  ptr->function->local_vars = (local_var *)0; /* Last var is NULL */
  ptr->next = (struct symrec *)sym_table;
  ptr->type = Fun;
  sym_table = ptr; 
  return ptr; 
}

symrec * putarr(char *arr_name, int size) {
  symrec *ptr;
  int i;
  ptr = (symrec *) malloc(sizeof(symrec));
  ptr->name = (char *) malloc(strlen(arr_name)+1); 
  strcpy (ptr->name,arr_name);
  ptr->array = (array *) malloc(sizeof(array));
  ptr->array->size = size;
  ptr->array->start_offset = data_location();
  for (i = 0; i < size - 1; i++) {
    data_location();
  }
  ptr->next = (struct symrec *)sym_table;
  ptr->type = Arr;
  sym_table = ptr; 
  return ptr; 
}
/************************** End Symbol Table **************************/ 

/*======================================================================== 
  Operations: Getlocalvar, Putlocalvar, Putparam
  ========================================================================*/ 
int getlocalvar(char *fun_name, char *var_name) {
  symrec * symrec;
  local_var * ptr;

  symrec = getsym(fun_name, Fun);
  if (symrec == NULL) {
    return FUN_NOT_EXIST;
  }
  for (ptr = symrec->function->local_vars;
       ptr != (local_var*)0;
       ptr = (local_var*) ptr->next) {
    if (strcmp(ptr->var_name, var_name) == 0) {
      /* returns position of var i stack */
      return ptr->offset;
    }
  }
  return LOC_VAR_NOT_EXIST;
}

int putlocalvar(char *fun_name, char *var_name) {
  /* Check if local variable or fun exists */
  int code;
  symrec * symrec;
  local_var * l_var;

  code = getlocalvar(fun_name, var_name);
  if (code >= OK) {
    return LOC_VAR_ALREADY_EXIST;
  }
  if (code == FUN_NOT_EXIST) {
    return code;
  }
  /* Get symrec of function name */
  symrec = getsym(fun_name, Fun);
  /* Increase number of variables */
  symrec->function->num_vars += 1;
  /* Memory management */
  l_var = (local_var *) malloc(sizeof(local_var));
  /* Assignment of var */
  l_var->var_name = strdup(var_name);
  l_var->offset = data_location();
  l_var->next = symrec->function->local_vars;
  /* Change local_vars of function */
  symrec->function->local_vars = l_var;
  return OK;
}

int putparam(char *fun_name, char *param_name) {
  /* Add parameter to local vars of fun */
  int code;
  symrec * symrec;
  code = putlocalvar(fun_name, param_name);
  if (code != OK) {
    return code;
  }
  /* Increase number of parameters */
  symrec = getsym(fun_name, Fun);
  symrec->function->num_params += 1;
  return OK; /*TODO: CHANGE THIS!!!*/
}

/************************** End Functions **************************/ 