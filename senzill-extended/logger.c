/*************************************************************************** 
Logger
Authors: Joel Cemeli
         Eduard Torres
***************************************************************************/ 

#include <stdio.h>
#include <stdarg.h>
#include "logger.h"

int verbosity;
int n_errors = 0;
int n_warns = 0;
int n_info = 0;

extern int include_stack_ptr;
extern int  numLineStack  [];
extern char* nameLineStack [];


void print_begin_info() {
    fprintf(stdout, KGRN "[INFO] " KNRM);
    fprintf(stdout, "Senzill compiler. Verbosity level: ");
    switch (verbosity) {
        case INFO:
            fprintf(stdout, KGRN "info" KNRM);
            break;
        case ERROR:
            fprintf(stdout, KRED "error" KNRM);
            break;
        default:
            fprintf(stdout, KYEL "warn" KNRM);
            break;
    }
    fprintf(stdout, "\n");
}

void print_info(const char *format, ...) {
    if (verbosity >= INFO) {
        va_list vargs;
        fprintf(stderr, KGRN "[INFO] " KNRM);
        va_start(vargs, format);
        vfprintf(stderr, format, vargs);
        fprintf(stderr, ".\n");
        va_end(vargs);
    }
    n_info++;
}

void print_warn(const char *format, ...) {
    if (verbosity >= WARN) {
        va_list vargs;
        fprintf(stderr, KYEL "[WARN] " KNRM);
        va_start(vargs, format);
        vfprintf(stderr, format, vargs);
        fprintf(stderr, ". Line: %d. File: %s\n", numLineStack[include_stack_ptr], nameLineStack[include_stack_ptr]);
        va_end(vargs);
    }
    n_warns++;
}

void print_error(const char *format, ...) {
    if (verbosity >= ERROR) {
        va_list vargs;
        fprintf(stderr, KRED "[ERROR] " KNRM);
        va_start(vargs, format);
        vfprintf(stderr, format, vargs);
        fprintf(stderr, ". Line: %d. File: %s\n", numLineStack[include_stack_ptr], nameLineStack[include_stack_ptr]);
        va_end(vargs);
    }
    n_errors++;
}

void print_final_info() {
    fprintf(stdout, KGRN "[INFO] " KNRM);
    fprintf(stdout, "Parser finished with ");
    fprintf(stdout, KRED "%d errors, " KNRM, n_errors);
    fprintf(stdout, KYEL "%d warnings, " KNRM, n_warns);
    fprintf(stdout, KGRN "%d info messages " KNRM, n_info);
    fprintf(stdout, "\n");
}
