/***************************************************************************
Scanner for the Simple language
Author: Anthony A. Aaby
Modified by: Jordi Planes
Extended by: Joel Cemeli
             Eduard Torres
***************************************************************************/

%option nounput
%option noinput

%x incl

%{
/*=========================================================================
C-libraries and Token definitions
=========================================================================*/
#include <string.h> /* for strdup */
#include <stdlib.h> /* for atoi */
#include "senzillc.tab.h" /* for token definitions and yylval */
#include "ST.h"
#include "logger.h"

/*============================ INCLUDE VAR ===========================*/
#define MAX_INCLUDE_DEPTH 10
YY_BUFFER_STATE include_stack[MAX_INCLUDE_DEPTH];
int include_stack_ptr = 0;

/*============================ STACKS ERROR ===========================*/
int  numLineStack  [MAX_INCLUDE_DEPTH] = { 1 };
char* nameLineStack [MAX_INCLUDE_DEPTH];

/*-------------------------------------------------------------------------
Path management
-------------------------------------------------------------------------*/
char * current_path;

char * get_real_path(char *file) {
  /* WARNING! Only works if file contains a relative path! */
  char *file_copy = strdup(file);
  char *final_path = malloc(strlen(file_copy)+strlen(current_path)+2);
  final_path[0] = '\0';
  strcat(final_path, current_path);
  strcat(final_path, "/");
  strcat(final_path, file_copy);
  return final_path;
}

%}

/*=========================================================================
TOKEN Definitions
=========================================================================*/
DIGIT [0-9]
ID [a-z]([a-zA-Z]|[0-9]|_)*



/*=========================================================================
REGULAR EXPRESSIONS defining the tokens for the Simple language
=========================================================================*/
%%
	/*============== COMMENTS ===============*/
"//".*\n  {}
"/*"(.|\n)*"*/"  {}

	/*============= INCLUDES ===============*/
#include BEGIN(incl);

<incl>[ \t]* {}
<incl>[^ \t\n]+ {
    /* Check for include max depth */
    if (include_stack_ptr >= MAX_INCLUDE_DEPTH) {
        print_error("Too much includes broo (including file %s)", yytext);
        exit(1);
    }
    /* Check if file has been included */
    if (getsym(yytext, Incl) == NULL) {
        char * real_path = get_real_path(yytext);
        putincl(real_path);
        include_stack[include_stack_ptr++] = YY_CURRENT_BUFFER;
        nameLineStack[include_stack_ptr] = strdup(real_path);
        yyin = fopen(real_path, "r");

        if (!yyin) {
            print_error("Error reading the file %s", yytext);
            exit(2);
        }
        yy_switch_to_buffer(yy_create_buffer(yyin, YY_BUF_SIZE));
        /* Incorporate include name to table of symbols */
    } else {
        print_warn("%s already included. Not included twice", yytext);
    }
    BEGIN(0);
 }

<<EOF>> {
    if (--include_stack_ptr < 0) {
        yyterminate();
    } else {
        yy_delete_buffer(YY_CURRENT_BUFFER);
        yy_switch_to_buffer(include_stack[include_stack_ptr]);
    }
 }

":=" { return(ASSGNOP); }
{DIGIT}+ { 
           yylval.intval = atoi( yytext );
           return(NUMBER);
         }
init {return(INIT);}
do { return(DO); }
else { return(ELSE); }
end { return(END); }
fi { return(FI); }
if { return(IF); }
in { return(IN); }
integer { return(INTEGER); }
let { return(LET); }
read { return(READ); }
skip { return(SKIP); }
then { return(THEN); }
while { return(WHILE); }
write { return(WRITE); }
return { return(RETURN); }
length { return(LENGTH); }
{ID} { yylval.id = strdup(yytext); return(IDENTIFIER); }
\n  { numLineStack[include_stack_ptr]++;}
[\ \t]+ /* eat up whitespace */
. { return(yytext[0]);}

%%

int yywrap(void){
   return 1;
}

/************************** End Scanner File *****************************/
