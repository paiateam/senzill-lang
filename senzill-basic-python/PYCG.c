/*************************************************************************** 
Python Code Generator 
Authors: Joel Cemeli
         Eduard Torres
***************************************************************************/ 

#include <stdio.h>
#include <string.h>
#include "SM.h"
#include "PYCG.h"

/*------------------------------------------------------------------------- 
Byte count
-------------------------------------------------------------------------*/ 
int num_bytes = 0;

/*------------------------------------------------------------------------- 
Singleton data
-------------------------------------------------------------------------*/ 
int raw_input_used = 0;
char raw_input_const[10] = "raw_input\0";
int raw_input_offset = 0;
char int_fun_const[4] = "int\0";
int int_fun_offset = 0;

int lastBackPatch = -1;

/*------------------------------------------------------------------------- 
Data Segment 
-------------------------------------------------------------------------*/ 
int names_offset = 0; /* Initial offset */ 
int py_data_location() /* Reserves a data location */ 
{ 
  return names_offset++; 
} 

/*------------------------------------------------------------------------- 
  Code Segment 
  -------------------------------------------------------------------------*/ 
int py_code_offset = 0;
int py_gen_label() /* Returns current offset */ 
{ 
  return py_code_offset-1; 
} 

int py_reserve_loc() /* Reserves a code location */ 
{ 
  _gen_py_code(POP_JUMP_IF_FALSE, 0);
  return py_code_offset-1; 
} 

/* Generates code at current location */ 
void py_gen_code( enum code_ops operation, int arg ) 
{ 
  switch(operation) {
    case HALT:
      _halt(arg);
      break;
    case STORE:
      _store(arg);
      break;
    case JMP_FALSE:
      _jumpFalse(arg);
      break;
    case GOTO:
      _jumpAbsolute(arg);
      break;
    case CALL:
      break;
    case RET:
      break;
    case DATA:
      break;
    case LD_INT:
      _ld_int(arg);
      break;
    case LD_VAR:
      _ld_var(arg);
      break;
    case READ_INT:
      _readInt(arg);
      break;
    case WRITE_INT:
      _write_int(arg);
      break;
    case LT:
    case EQ:
    case GT:
      _compareOp(operation);
      break;
    case ADD:
    case SUB:
    case MULT:
    case DIV:
      _addOp(operation);
      break;
    case PWR:
      break;
    case LD_SUB:
      break;
    case STORE_SUB:
      break;
  }
} 

/* Generates a new name (only useful at PYCG) */
/* PRECONDITION: name has not been added to ST previously */
void py_gen_name(char * name) {
  py_names[names_offset].name = strdup(name);
  py_names[names_offset].offset = names_offset;
}

/* Generates code at a reserved location */ 
void py_back_patch( int addr, enum code_ops operation, int arg ) 
{ 
  switch(operation){
    case JMP_FALSE:
      py_code[addr].op = POP_JUMP_IF_FALSE;
      break;
    case GOTO:
      py_code[addr].op = JUMP_ABSOLUTE;
      break;
    default:
      return;
  }
  lastBackPatch = addr;
}

/*------------------------------------------------------------------------- 
  Constant Segment 
  -------------------------------------------------------------------------*/ 
int const_offset = 0;

/*------------------------------------------------------------------------- 
Print Code to stdio 
-------------------------------------------------------------------------*/ 
void py_print_code() 
{ 
  int i;
  printf("INSTRUCTIONS:\n\n");
  for (i = 0; i < py_code_offset; ++i) {
    printf("%d %d\n", py_code[i].op, py_code[i].arg);
  }
  printf("\n");
  printf("CONSTANTS:\n\n");
  for (i = 0; i < const_offset; ++i) {
    printf("%d %d\n", py_constants[i].value, py_constants[i].offset);
  }
  printf("\n");
  printf("NAMES:\n\n");
  for (i = 0; i < names_offset; ++i) {
    printf("%s %d\n", py_names[i].name, py_names[i].offset);
  }
} 

/* Reads code from file */

void py_read_bytecode( char *file_name ) {
  
}

/*------------------------------------------------------------------------- 
Write bytecode to file
-------------------------------------------------------------------------*/ 

/* Swap bytes to write file in little endian */
int byteswapint(int input) {
  int output = (int) input;
  output = (output & 0x0000FFFF) << 16 | (output & 0xFFFF0000) >> 16;
  output = (output & 0x00FF00FF) << 8 | (output & 0xFF00FF00) >> 8;
  return output;
}

/* Write 'amount' separators to file  */
void write_separator(FILE* ptr_file, int amount) {
  int i;
  unsigned char byte = 0;
  for (i = 0; i < amount; ++i) {
    fwrite((const void*) &byte, 1, 1, ptr_file);
  }
}

/* Writes code to file */
void py_write_bytecode( char *file_name ) {
  FILE *ptr_file;
  ptr_file = fopen(file_name, "wb");
  if (!ptr_file) {
    printf("Unable to open the file!");
    return;
  }

  _write_header(ptr_file);
  _write_instructions(ptr_file);
  _write_constants(ptr_file);
  _write_names(ptr_file);
  _write_common_footer(ptr_file);
  _write_filename(ptr_file);
  _write_module(ptr_file);
  _write_end_footer(ptr_file);
  
  /* Close file */
  fclose(ptr_file);
}

/************************** End Code Generator **************************/ 

/************************** Private Functions **************************/

/*------------------------------------------------------------------------- 
Translation of Senzill instructions to Python Bytecode instructions
-------------------------------------------------------------------------*/ 
void _gen_py_code(enum PyCodes code, int arg) {
  py_code[py_code_offset].bytePos = num_bytes;
  py_code[py_code_offset].op = code;
  py_code[py_code_offset].arg = arg;
  py_code_offset++;
  if(lastBackPatch != -1){
    py_code[lastBackPatch].arg = num_bytes;
    lastBackPatch = -1;
  }
  if (arg != -1) {
    num_bytes += 3;
  } else {
    num_bytes += 1;
  }

}

void _store(int arg) {
  _gen_py_code(STORE_NAME, arg);
}

void _ld_int(int arg) {
  py_constants[const_offset].value = arg;
  py_constants[const_offset].offset = const_offset;

  _gen_py_code(LOAD_CONST, const_offset);
  const_offset++;
}

void _write_int(int arg) {
  _gen_py_code(PRINT_ITEM, -1);
  _gen_py_code(PRINT_NEWLINE, -1);
}

void _ld_var(int arg) {
  _gen_py_code(LOAD_NAME, arg);
}

void _halt(int arg) {
  /* Load None. Will be stored at the end of the program */
  _gen_py_code(LOAD_CONST, const_offset);
  _gen_py_code(RETURN_VALUE, -1);
}

void _addOp(enum code_ops operation){
  enum PyCodes code;
  switch(operation){
    case ADD:
      code = BINARY_ADD;
      break;
    case SUB:
      code = BINARY_SUBTRACT;
      break;
    case MULT:
      code = BINARY_MULTIPLY;
      break;
    case DIV:
      code = BINARY_DIVIDE;
      break;
    default:
      return;
  }
  _gen_py_code(code, -1);
}

void _readInt(int arg){
  if(raw_input_used == 0){
    py_gen_name(raw_input_const);
    raw_input_offset = names_offset++;
    py_gen_name(int_fun_const);
    int_fun_offset = names_offset++;
    raw_input_used++;
  }
  _ld_var(int_fun_offset);
  _ld_var(raw_input_offset);
  _callFunction(0);
  _callFunction(1);
  _store(arg);
}

void _callFunction(int arg){
  _gen_py_code(CALL_FUNCTION, arg);
}

void _compareOp(enum code_ops code){
  enum PyComparationCodes comparator;
  switch(code){
    case LT:
      comparator = LESS;
      break;
    case EQ:
      comparator = EQUALS;
      break;
    case GT:
      comparator = GREATER;
      break;
    default:
      return;
  }
  _gen_py_code(COMPARE_OP, comparator);
}

void _jumpFalse(int arg){
  _gen_py_code(POP_JUMP_IF_FALSE, arg);
}

void _jumpAbsolute(int arg){
  int jumpPos = 0;
  if(arg < py_code_offset) {
    jumpPos = py_code[arg+1].bytePos; 
  }
  _gen_py_code(JUMP_ABSOLUTE, jumpPos);
}

/*------------------------------------------------------------------------- 
Auxiliary functions of write_bytecode
-------------------------------------------------------------------------*/ 
void _write_header(FILE * ptr_file) {
  int magic = 66260234;
  int date = 4220921177;
  int aux;
  unsigned char byte;
  
  /* Write magic number */
  aux = byteswapint(magic);
  fwrite((const void*) &aux, sizeof(int), 1, ptr_file);

  /* Write date */
  aux = byteswapint(date);
  fwrite((const void*) &aux, sizeof(int), 1, ptr_file);

  /* Write other header stuff */
  byte = 99; /* indicator */
  fwrite((const void*) &byte, 1, 1, ptr_file);
  write_separator(ptr_file, 8);
  byte = 2; /* stacksize */
  fwrite((const void*) &byte, 1, 1, ptr_file);
  write_separator(ptr_file, 3);
  byte = 64; /* flags */
  fwrite((const void*) &byte, 1, 1, ptr_file);
  write_separator(ptr_file, 3);
}

void _write_instructions(FILE * ptr_file) {
  unsigned char byte;
  short byte_2;
  int i;
  
  /* Number of bytes of instructions */
  byte = 115;
  fwrite((const void*) &byte, 1, 1, ptr_file);
  fwrite((const void*) &num_bytes, sizeof(int), 1, ptr_file);

  /* Write bytecode of instructions */
  for (i = 0; i < py_code_offset; ++i) {
    byte = py_code[i].op;
    fwrite((const void*) &byte, 1, 1, ptr_file);
    if (py_code[i].arg != -1) {
      byte_2 = py_code[i].arg;
      fwrite((const void*) &byte_2, 2, 1, ptr_file);
    }
  }
}

void _write_constants(FILE * ptr_file) {
  unsigned char byte;
  int num_const;
  int i;

  /* Write constants */
  byte = 40;

  num_const = const_offset + 1;
  fwrite((const void*) &byte, 1, 1, ptr_file); /* indicator */
  fwrite((const void*) &num_const, sizeof(int), 1, ptr_file); /* Num constants */

  byte = 105;
  for (i = 0; i < const_offset; ++i) {
    fwrite((const void*) &byte, 1, 1, ptr_file); /* indicator */
    fwrite((const void*) &py_constants[i].value,
           sizeof(int), 1, ptr_file); /* const value */
  }
  byte = 78; /* None */
  fwrite((const void*) &byte, 1, 1, ptr_file);
}

void _write_names(FILE * ptr_file) {
  unsigned char byte;
  int i;
  
  /* Write names */
  byte = 40;
  fwrite((const void*) &byte, 1, 1, ptr_file); /* indicator */
  fwrite((const void*) &names_offset, sizeof(int), 1, ptr_file); /* Num names */

  byte = 116;
  for (i = 0; i < names_offset; ++i) {
    int str_length;
    int j;
    fwrite((const void*) &byte, 1, 1, ptr_file); /* string indicator */
    str_length = strlen(py_names[i].name);
    fwrite((const void*) &str_length, sizeof(int), 1, ptr_file); /* length of string */
    for (j = 0; j < str_length; ++j) { /* string */
      fwrite((const void*) &py_names[i].name[j], 1, 1, ptr_file); 
    }
  }
}

void _write_common_footer(FILE * ptr_file) {
  unsigned char byte;
  int i;

  /* varnames, freevars and cellvars */
  byte = 40;
  for (i = 0; i < 3; ++i) {
    fwrite((const void*) &byte, 1, 1, ptr_file); /* indicator */
    write_separator(ptr_file, 4);
  }
}

void _write_filename(FILE * ptr_file) {
  unsigned char byte;
  int aux;
  char in_name[5] = "in.sz";
  int i;

  /* filename (TODO: put real filename) */
  byte = 115;
  fwrite((const void*) &byte, 1, 1, ptr_file);
  aux = 5;
  fwrite((const void*) &aux, sizeof(int), 1, ptr_file);
  for (i = 0; i < 5; ++i) {
    fwrite((const void*) &in_name[i], 1, 1, ptr_file); 
  }
}

void _write_module(FILE * ptr_file) {
  unsigned char byte;
  int aux;
  char module[8] = "<module>";
  int i;
  
  /* module */
  byte = 116;
  fwrite((const void*) &byte, 1, 1, ptr_file);
  aux = 8;
  fwrite((const void*) &aux, sizeof(int), 1, ptr_file);
  for (i = 0; i < 8; ++i) {
    fwrite((const void*) &module[i], 1, 1, ptr_file); 
  }
}

void _write_end_footer(FILE * ptr_file) {
  int aux;
  unsigned char byte;
  
  /* firstlineno */
  aux = 1;
  fwrite((const void*) &aux, sizeof(int), 1, ptr_file);

  /* lnotab */
  byte = 115;
  fwrite((const void*) &byte, 1, 1, ptr_file);
  write_separator(ptr_file, 4);
}