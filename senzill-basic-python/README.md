# Senzill Language (Basic Python Version) #

Extension added to basic *senzill* language to allow compilation to Python bytecode that can be executed using **Python 2.7**.

## Summary ##

We have generated a new file called *PYCG* (**PY**thon **C**ode **G**enerator) that tries to follow *CG* (**C**ode **G**enerator) interface, including all the logic needed to create Python bytecode here. This is used by *Yacc* (**Y**et **A**nother **C**ompiler of **C**ompilers) using *CGSwitch*, that can alternate between *PYCG* and *CG*.

Using the flag **-p** when executing *senzill* compiler we can force to use Python compilation.

## Compilation ##

This project can be compiled using the provided **Makefile**.

## Execution ##
```
    ./senzillc -i <input_file> -o <output_file> [-v <verb-level>] [-p]
```
To learn more about the optional flags execute *senzill* without any parameter.

## Syntax of *senzill* ##

We have not modified *senzill* syntax. Syntax used in the other version of *senzill* is slightly different. Check its own README to learn more about it.

## Structure of Python 2.7 Bytecode ##

We have used Python versions 2.7.9, 2.7.12 and 2.7.13. It is known that Python bytecode changes on every Python version. We recommend to use any of this versions, although it might work in any 2.7.X.

**NOTE:** All the following information has been obtained applying reverse engineering to Python bytecode, using **od** Unix tool and also a disassembler Python script that translates some bytes to a more readable format. If you want to generate python bytecode, you can follow this guide and visit [kiranbkrishna](https://kiranbkrishna.wordpress.com/2013/03/20/playing-with-python-bytecode/) and [nedbatchelder](https://nedbatchelder.com/blog/200804/the_structure_of_pyc_files.html) websites.

The example shown below belongs to the decompilation of this code:
```Python
b = 3
a = 2
print a + b
```

We can compile this code and obtain this hexadecimal version:

```
03 f3 0d 0a fb 96 25 59 63 00 00 00 00 00 00 00
00 02 00 00 00 40 00 00 00 73 19 00 00 00 64 00
00 5a 00 00 64 01 00 5a 01 00 65 01 00 65 00 00
17 47 48 64 02 00 53 28 03 00 00 00 69 03 00 00
00 69 02 00 00 00 4e 28 02 00 00 00 74 01 00 00
00 62 74 01 00 00 00 61 28 00 00 00 00 28 00 00
00 00 28 00 00 00 00 73 09 00 00 00 6d 79 73 75
6d 32 2e 70 79 74 08 00 00 00 3c 6d 6f 64 75 6c
65 3e 01 00 00 00 73 04 00 00 00 06 01 06 01
```

**IMPORTANT**: As one can see, all this numbers are in hexadecimal notation, because they are representing bytes!

We will analyze this bytecode providing indications about the different sections and what are representing this bytes.

Let's start!

### Header ###

Header contains some common information used by Python virtual machine.

```
03 f3 0d 0a (magic)
fb 96 25 59  (date)

63 (flags)
00 00 00 00 00 00 
00 00 02 00 00 00 40
00 00 00 
```

* 4 Bytes: Magic number. This number is unique for every Python version, and it is used to check for incompatibilities. Encoded in *little endian*.
* 4 Bytes: Date of modification. Encoded in *little endian*.
* 17 Bytes: Flags. This include number of arguments (not useful here), number of local variables (idem), stack size and other flags.

In our case all of this code will be common (even the modification date).

### Body ###
In the body we can find the encoded instructions, constants and variable declaration.

#### Instructions ####

```
73 19 00 00 00 (number of instructions)

64 00 00 
5a 00 00 
64 01 00 
5a 01 00 
65 01 00 
65 00 00
17 
47 
48 
64 02 00 
53
```


* First of all, we have a 0x73 followed by a 4-byte integer with the number of total bytes of instructions. Note that this integer is swapped (0x19 is 25, and there are 25 bytes of instructions). This is due _little endian_ and _big endian_ conversions. In our case, this fact is not very important; we only have to write bytes in the same order that they appear here.
* After that, we can find all the instructions. There are instructions with and without argument. This argument will be of 2 bytes and it is also swapped. Usually it will be used to access to constants or names.

We have used a reduced subset of all the instructions that Python bytecode offers. All of them can be found in *PYCG* file declared as an *enum* with their code in decimal.

#### Constants ####

```
28 03 00 00 00 (number constants)
69 03 00 00 00 (first constant)
69 02 00 00 00 (second constant) 
4e (third constant -> None)
```

* In a simmilar way that with the instructions, we have a 0x28 followed with the number of constants.
* Every integer constant is represented by a 0x69 and its value (another time, this value and the number of constants are swapped). In our case, all the variables that we will use will be integers.
* __None__ is represented by the value 0x4e

#### Names ####

In Python, all the variables have a name. We have to declare them here.

```
28 02 00 00 00 (number names) 
74 01 00 00 00 (string size 1) 
62 ("b")
74 01 00 00 00 (string size 1) 
61 ("a")
```


* As before, we have to declare the number of names that we have.
* 0x74 is followed by the number of characters that this name has.
* Then, there are the ASCII representation of this string.
* Repeat with all the names of the program.

#### Other variables ####

There are 3 more spaces to other variables that we have not used. They are *varnames*, *freevars* and *cellvars*; they are declared starting with a 0x28 and the number of items (in our case, 0).

### Footer ###

At the end of the file, we can find the name of the file, at which module belongs and line counting information.

#### File name ####

```
73 09 00 00 00 (string size 9) 
6d 79 73 75 6d 32 2e 70 79 (mysum2.py)
```

* 0x73 followed by the size of the file name.
* File name encoded in ASCII.

In our case will always be *in.sz*. In the future this hardcoded string could be changed.

#### Module ####

Used in case that this code is part of a module. in our case will be always *<module>*.

```
74 08 00 00 00 (string size 8)
3c 6d 6f 64 75 6c 65 3e ("<module>")
```

* 0x74 followed by the length of module string.
* Module name (*<module>* if it is main)

#### Line counting ####

This information is used by Python virtual machine to show error at runtime. We have hardcoded this part, and in the future this can be extended using actual line numbers.

```
01 00 00 00 (firstlineno)

73 04 00 00 00 (lnotab)
06 01 06 01
```

* *firstlineno* is the first line of the program.
* *lnotab* is the relationship between bytes and actual line numbers of python code.
