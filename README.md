# SENZILL LANGUAGE #

*Senzill* is a programming language developed at [Language Processing Algorithms](http://guiadocent.udl.cat/pdf/en/102043) course at [University of Lleida](http://www.udl.es/ca/en/).

Basic version of *Senzill* has been developed by Anthony A. Aaby and modified by Jordi Planes.

[Joel Cemeli](https://www.linkedin.com/in/joel-cemeli-00a037a1) and [Eduard Torres](https://www.linkedin.com/in/eduard-torres-montiel-75b9a2108) have extended this initial version, including all the features that will be explained below, and also a Python bytecode generator.

## Implemented Features ##

Two different implementations are provided:

* [senzill-extended](https://bitbucket.org/paiateam/senzill-lang/src/6289f66e1a7ff8390b4dd80f7c6061e0519f4b8d/senzill-extended/?at=master) - Extended version of *senzill* that includes:
    * Modification of grammar
    * Functions (declaration, implementation and invocation)
    * Static arrays
    * Error control and basic (and colourful) logging system
    * Comments treating
    * Inclusion of functions declared and implemented in other files

* [senzill-basic-python](https://bitbucket.org/paiateam/senzill-lang/src/6289f66e1a7ff8390b4dd80f7c6061e0519f4b8d/senzill-basic-python/?at=master) - Basic initial version of *senzill* with the epic-fantastic feature of generating Python bytecode. Also includes basic error control and basic (and colourful) logging system.

In order to learn more about any of this versions check their own markdown file.