/*************************************************************************** 
  Code Generator
***************************************************************************/ 

#ifndef __CG_H
#define __CG_H

#include "SM.h"

extern int sz_code_offset;

int  sz_data_location();
void sz_gen_code( enum code_ops operation, int arg );
void sz_gen_name(char * name); /* Only used to keep CGSwitch interface */
int  sz_reserve_loc();
void sz_back_patch( int addr, enum code_ops operation, int arg );
int  sz_gen_label();
void sz_print_code();
void sz_read_bytecode( char *file_name );
void sz_write_bytecode( char *file_name );

#endif
