/*------------------------------------------------------------------------- 
SYMBOL TABLE RECORD 
-------------------------------------------------------------------------*/ 

/* ERROR CODES */
#define FUN_NOT_EXIST -1
#define LOC_VAR_NOT_EXIST -2
#define LOC_VAR_ALREADY_EXIST -3
#define FUN_ALREADY_EXIST -4
#define WRONG_NUM_PARAMS -5
#define ARR_OUT_OF_BOUNDS -6
#define UNDECLARED -7
#define OK 0

/* TYPES */
enum Type {Var, Incl, Fun, Arr};

/* Array struct */
struct array {
  int start_offset;
  int size;
};
typedef struct array array;

/* Function struct */
struct function
{
  int num_params;
  int num_vars;
  int ret_offset;
  struct local_var *local_vars;
};
typedef struct function function;

/* Function local var struct */
struct local_var
{
  char *var_name;
  int offset;
  struct local_var *next;
};
typedef struct local_var local_var;

/* Symrec struct */
struct symrec 
{ 
  char *name; /* name of symbol */ 
  int offset; /* data offset */ 
  struct function *function; /* in case we have a function */
  struct array *array; /* in case we have an array */
  struct symrec *next; /* link field */
  enum Type type; /* type of symbol */
}; 
typedef struct symrec symrec;

/******************* Symrec functionality ********************/
symrec * getsym (char *sym_name, enum Type type);

/******************* Var functionality ********************/
symrec * putvar(char *var_name);

/******************* Include functionality ********************/
symrec * putincl(char *incl_name);

/******************* Function functionality ********************/
symrec * putfun(char *fun_name);
int getlocalvar(char *fun_name, char *var_name);
int putlocalvar(char *fun_name, char *var_name);
int putparam(char *fun_name, char *param_name);

/******************* Array functionality ********************/
symrec * putarr(char *arr_name, int size);