/*************************************************************************** 
Code Generator Switch
***************************************************************************/
#ifndef __CGS__
#define __CGS__

#include "SM.h"

extern int sz_compilation;

void compile_to_py();
void compile_to_sz();
int is_sz_compilation();

int  (*data_location)(void);
void (*gen_code)(enum code_ops, int);
void (*gen_name)(char *);
int  (*reserve_loc)(void);
void (*back_patch)(int, enum code_ops, int);
int  (*gen_label)(void);
void (*print_code)(void);
void (*read_bytecode)(char *);
void (*write_bytecode)(char *);

#endif