%{
/*************************************************************************
Compiler for the Simple language
Author: Anthony A. Aaby
Modified by: Jordi Planes
Extended by: Joel Cemeli
             Eduard Torres
***************************************************************************/
/*=========================================================================
C Libraries, Symbol Table, Code Generator & other C code
=========================================================================*/
#include <stdio.h> /* For I/O */
#include <stdlib.h> /* For malloc here and in symbol table */
#include <string.h> /* For strcmp in symbol table */
#include <libgen.h> /* For path treatment */
#include "ST.h" /* Symbol Table */
#include "SM.h" /* Stack Machine */
#include "CG.h" /* Code Generator */
#include "logger.h" /* Logginc custom file */

#define YYDEBUG 1 /* For Debugging */

extern int verbosity;
extern int n_errors;
extern FILE *yyin;
extern int include_stack_ptr;
extern int  numLineStack  [];
extern char* nameLineStack [];

char * lastFunction;
extern char * current_path;

int yyerror(char *);
int yylex();
void funError(int errorCode, char * name);
void print_help();

int num_params; /* Num params of function */
/*-------------------------------------------------------------------------
The following support backpatching
-------------------------------------------------------------------------*/
struct lbs /* Labels for data, if and while */
{
  int for_goto;
  int for_jmp_false;
};

struct lbs * newlblrec() /* Allocate space for the labels */
{
   return (struct lbs *) malloc(sizeof(struct lbs));
}

/*-------------------------------------------------------------------------
Install identifiers & check if previously defined.
-------------------------------------------------------------------------*/
void install_var(char *sym_name) {
  symrec *s = getsym (sym_name, Var);
  if (s == 0) {
    s = putvar(sym_name);
  } else {
    print_error("Variable %s is already defined\n", sym_name);
  }
}

void install_fun(char *sym_name) {
  symrec *s = getsym (sym_name, Fun);
  if (s == 0) {
    s = putfun(sym_name);
  } else {
    print_error("Function %s is already defined\n", sym_name);
  }
}

void install_arr(char *sym_name, int size) {
  symrec *s = getsym(sym_name, Arr);
  if (s == 0) {
    s = putarr(sym_name, size);
  } else {
    print_error("Array %s is already defined\n", sym_name);
  }
}

/*-------------------------------------------------------------------------
If identifier is defined, generate code
-------------------------------------------------------------------------*/
int context_check( char *sym_name, enum Type type )
{
  symrec *identifier = getsym( sym_name, type );
  if (identifier == NULL) {
    return UNDECLARED;
  }
  return identifier->offset;
}

int arr_context_check(char *sym_name, int pos) {
  symrec *identifier = getsym(sym_name, Arr);
  if (identifier == NULL) {
    return UNDECLARED;
  }
  if (identifier->array->size <= pos) {
    return ARR_OUT_OF_BOUNDS;
  }
  return identifier->array->start_offset + pos;
}

/*-------------------------------------------------------------------------
Path management
-------------------------------------------------------------------------*/
void add_current_path(char *file) {
  char *file_copy = strdup(file);
  current_path = dirname(file_copy);
}

/*=========================================================================
SEMANTIC RECORDS
=========================================================================*/
%}

%union /* semrec - The Semantic Records */
 {
   int intval; /* Integer values */
   char *id; /* Identifiers */
   struct lbs *lbls; /* For backpatching */
};

/*=========================================================================
TOKENS
=========================================================================*/
%start program
%token INIT
%token <intval> NUMBER /* Simple integer */
%token <id> IDENTIFIER /* Simple identifier */
%token <lbls> IF WHILE /* For backpatching labels */
%token SKIP THEN ELSE FI DO END
%token INTEGER READ WRITE LET IN RETURN LENGTH
%token ASSGNOP

/*=========================================================================
OPERATOR PRECEDENCE
=========================================================================*/
%left '-' '+'
%left '*' '/'
%right '^'

/*=========================================================================
GRAMMAR RULES for the Simple language
=========================================================================*/

%%

program : INIT { data_location(); data_location(); gen_code(GOTO, 0); } func_list LET declarations_l IN { back_patch(0, GOTO, gen_label()); gen_code( DATA, data_location() - 1 );}
          commands END { gen_code( HALT, 0 ); YYACCEPT; }
          | error END {print_error("Bad structured program, remember: init -> <functions|includes> -> let -> <declarations> -> in -> <main> -> end ");
            exit(1);}
;

func_list : func_list func_impl { print_info("Function found"); }
          | /*empty*/
;

/****************** FUNCTIONS *******************/
func_head : INTEGER IDENTIFIER {install_fun($2); lastFunction = strdup($2);} '(' param_seq ')' 
;

param_seq : param_seq IDENTIFIER {
              print_info("Let's add param to function %s", lastFunction);
              int result = putparam(lastFunction, $2); 
              if(result >= OK){
                print_info("Parameter added to function %s", lastFunction);
              }
              else{
                funError(result, $2);
              }
            }
          | /* empty */
;

func_impl : func_head DO func_commands_l END {print_info("Function read");}
          | error DO {print_error("Bad function header"); yyerrok;}
          | error END {print_error("Bad declared function"); yyerrok;}
;

func_commands_l: func_commands_l func_commands
               | func_commands
;

func_commands : INTEGER IDENTIFIER ';' {
                print_info("Let's add local var to function %s", lastFunction); 
                int result = putlocalvar(lastFunction, $2); 
                if(result >= OK){
                  print_info("Added localvar to function %s", lastFunction);
                }
                else{
                  funError(result, $2);
                }
              }
              | fun_command ';'
              | RETURN fun_exp ';' {
                  int def = getsym(lastFunction, Fun)->function->ret_offset;
                  gen_code( STORE,    def);
                  gen_code( RET,      0);
                }
              | IF fun_bool_exp { $1 = (struct lbs *) newlblrec(); $1->for_jmp_false = reserve_loc(); }
                THEN func_commands_l { $1->for_goto = reserve_loc(); } 
                ELSE {back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() );} func_commands_l
                FI { back_patch( $1->for_goto, GOTO, gen_label() );}
              | WHILE { $1 = (struct lbs *) newlblrec(); $1->for_goto = gen_label(); }
                fun_bool_exp { $1->for_jmp_false = reserve_loc(); } DO func_commands_l END { gen_code( GOTO, $1->for_goto );
                back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() ); }
              | error ';' {print_error("Unrecognized command"); yyerrok;}
;

func_usage : IDENTIFIER {
                         num_params = 0;
                         lastFunction = strdup($1);
                         symrec * symrec = getsym($1, Fun);
                         if (symrec == NULL) {
                           print_error("Function %s undeclared", $1);
                         }
                        }
            '(' func_params_usage ')' {  symrec * symrec = getsym($1, Fun);
                                         if (symrec != NULL) {
                                          if (num_params != symrec->function->num_params) {
                                            print_error("Function %s called with wrong number of parameters. Expected: %d; Actual: %d",
                                                        $1, symrec->function->num_params, num_params);
                                            }
                                            gen_code( CALL, symrec->offset);
                                            gen_code( LD_VAR, symrec->function->ret_offset);
                                         }
                                       }
;

usage_exp : NUMBER { gen_code( LD_INT, $1 ); }
   | IDENTIFIER { int offset = context_check($1, Var);
                  if (offset == UNDECLARED) {
                    print_error("Variable %s undeclared", $1);
                  } else {
                    gen_code( LD_VAR, offset);
                  }
                }
   | usage_exp '+' usage_exp { gen_code( ADD, 0 ); }
   | usage_exp '-' usage_exp { gen_code( SUB, 0 ); }
   | usage_exp '*' usage_exp { gen_code( MULT, 0 ); }
   | usage_exp '/' usage_exp { gen_code( DIV, 0 ); }
   | usage_exp '^' usage_exp { gen_code( PWR, 0 ); }
   | '(' usage_exp ')'
;


fun_command : SKIP
   | READ IDENTIFIER { 
      int pos = getlocalvar(lastFunction, $2);
      if(pos < 0){
        print_error("Variable %s not found in function %s", $2, lastFunction);
      }
      else{
        gen_code( READ_INT, pos);
      }
     }
   | WRITE fun_exp { 
     gen_code( WRITE_INT, 0 );
   }
   | IDENTIFIER ASSGNOP fun_exp {
      int pos = getlocalvar(lastFunction, $1);
      if(pos < 0){
        print_error("Variable %s not found in function %s", $1, lastFunction);
      }
      else{
        gen_code( STORE, pos); 
      }
      }
;

func_params_usage : func_params_usage usage_exp {
                    num_params++;
                    symrec *fun = getsym(lastFunction, Fun);
                    if (fun != NULL) {
                      int def = fun->function->ret_offset + num_params;
                      gen_code( STORE, def);
                    }
                  }
                  | /*empty*/
;

fun_bool_exp : fun_exp '<' fun_exp { gen_code( LT, 0 ); }
   | fun_exp '=' fun_exp { gen_code( EQ, 0 ); }
   | fun_exp '>' fun_exp { gen_code( GT, 0 ); }
;

fun_exp : NUMBER { gen_code( LD_INT, $1 ); }
   | IDENTIFIER {
      int pos = getlocalvar(lastFunction, $1);
      if(pos < 0){
        print_error("Variable %s not found in function %s", $1, lastFunction);
      }
      else{
        gen_code(LD_VAR, pos);
      }
     }
   | fun_exp '+' fun_exp { gen_code( ADD, 0 ); }
   | fun_exp '-' fun_exp { gen_code( SUB, 0 ); }
   | fun_exp '*' fun_exp { gen_code( MULT, 0 ); }
   | fun_exp '/' fun_exp { gen_code( DIV, 0 ); }
   | fun_exp '^' fun_exp { gen_code( PWR, 0 ); }
   | '(' fun_exp ')'
;

declarations_l : /* empty */
               | declarations_l declarations

declarations : INTEGER id_seq ';' {}
             | INTEGER arr_seq ';' {}
             | error ';' {print_error("Bad declaration", numLineStack[include_stack_ptr], nameLineStack[include_stack_ptr]); yyerrok;}
;

id_seq : IDENTIFIER { install_var( $1 ); }
    | id_seq IDENTIFIER { install_var( $2 ); }
;

arr_seq : IDENTIFIER '[' NUMBER ']' {
                                     if ($3 < 1) {
                                       print_error("Invalid argument for array constuction: %d", $3); 
                                     } else {
                                       install_arr($1, $3);
                                     }
                                  }
    | arr_seq IDENTIFIER '[' NUMBER ']' {if ($4 < 1) {
                                          print_error("Invalid argument for array constuction: %d", $2); 
                                        } else {
                                          install_arr($2, $4);
                                        }
                                  }
;

commands : /* empty */
    | commands command ';'
    | error ';' {print_error("Bad command", numLineStack[include_stack_ptr], nameLineStack[include_stack_ptr]); yyerrok;}
;

command : SKIP
   | READ IDENTIFIER {  int offset = context_check($2, Var);
                        if (offset == UNDECLARED) {
                          print_error("Variable %s undeclared", $2);
                        } else {
                          gen_code(READ_INT, offset);
                        }
                      }
   | WRITE exp { gen_code( WRITE_INT, 0 ); }
   | IDENTIFIER ASSGNOP exp { int offset = context_check($1, Var);
                              if (offset == UNDECLARED) {
                                print_error("Variable %s undeclared", $1);
                              } else {
                                gen_code( STORE, offset );
                              }
                            }
   | IDENTIFIER '[' exp ']' ASSGNOP exp {int offset = arr_context_check($1, 0);
                                            if (offset >= OK) {
                                              gen_code(STORE, 0); /*Store to temp position*/
                                              gen_code(STORE, 1);
                                              gen_code(LD_VAR, 0);
                                              gen_code(LD_VAR, 1);
                                              gen_code(STORE_SUB, offset);
                                            } else {
                                              print_error("Array %s not declared", $1);
                                            }
                                          }
   | IF bool_exp { $1 = (struct lbs *) newlblrec(); $1->for_jmp_false = reserve_loc(); }
   THEN commands { $1->for_goto = reserve_loc(); } ELSE {
     back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() );
   } commands FI { back_patch( $1->for_goto, GOTO, gen_label() ); }
   | WHILE { $1 = (struct lbs *) newlblrec(); $1->for_goto = gen_label(); }
   bool_exp { $1->for_jmp_false = reserve_loc(); } DO commands END { gen_code( GOTO, $1->for_goto );
   back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() ); }
;

bool_exp : exp '<' exp { gen_code( LT, 0 ); }
   | exp '=' exp { gen_code( EQ, 0 ); }
   | exp '>' exp { gen_code( GT, 0 ); }
;

exp : NUMBER { gen_code( LD_INT, $1 ); }
   | IDENTIFIER { int offset = context_check($1, Var);
                  if (offset == UNDECLARED) {
                    print_error("Variable %s undeclared", $1);
                  } else {
                    gen_code( LD_VAR, offset );
                  }
                }
   | IDENTIFIER '[' exp ']' {int offset = arr_context_check($1, 0);
                                if (offset >= OK) {
                                  gen_code(LD_SUB, offset);
                                } else {
                                  print_error("Array %s not declared", $1);
                                }
                              }
   | LENGTH IDENTIFIER {  symrec * arr = getsym($2, Arr);
                          if (arr == NULL) {
                            print_error("Array %s undefined", $2);
                          } else {
                            gen_code(LD_INT, arr->array->size);
                          }
                        }
   | exp '+' exp { gen_code( ADD, 0 ); }
   | exp '-' exp { gen_code( SUB, 0 ); }
   | exp '*' exp { gen_code( MULT, 0 ); }
   | exp '/' exp { gen_code( DIV, 0 ); }
   | exp '^' exp { gen_code( PWR, 0 ); }
   | '(' exp ')'
   | func_usage {}
;

%%

extern struct instruction code[ MAX_MEMORY ];

/*=========================================================================
MAIN
=========================================================================*/
int main( int argc, char *argv[] )
{
  if ( argc < 3 || argc > 5 ) {
    print_help();
    return -1;
  }
  if (argc > 3) {
    /* Expecting debug level */
    if (strcmp(argv[3], "-v") == 0 || strcmp(argv[3], "--verbose") == 0) {
      /* Good keyword, proceed to find debug level */
      if (strcasecmp(argv[4], "info") == 0) {
        verbosity = INFO;
      } else if (strcasecmp(argv[4], "warn") == 0) {
        verbosity = WARN;
      } else if (strcasecmp(argv[4], "error") == 0) {
        verbosity = ERROR;
      } else {
        print_error("Unreconized option: %s\n", argv[4]);
        print_help();
        return -1;
      }
    } else {
      /* Wrong keyword */
      print_error("Unrecognized keyword: %s\n", argv[3]);
      print_help();
      return -1;
    }
  } else {
    /* Default verbosity level */
    verbosity = WARN;
  }
  /* Put main file to include symbol tab.
     Only works if this name is equals to the name in equals */
  putincl(argv[1]);
  add_current_path(argv[1]);
  nameLineStack[0] = strdup(argv[1]);
  yyin = fopen( argv[1], "r" );
  if (yyin == NULL) {
    print_error("File %s not found\n", argv[1]);
    return -2;
  }
  /*yydebug = 1;*/
  print_begin_info();
  yyparse ();
  print_final_info();
  if ( n_errors == 0 )
    {
      //print_code ();
      //fetch_execute_cycle();
      write_bytecode( argv[2] );
    }
  return 0;
}

void print_help() {
  printf("usage <input-file> <output-file> [-v|--verbosity] <verb-level>\n");
  printf("verb-level:\n");
  printf(" - info\n");
  printf(" - warn\n");
  printf(" - error\n");
  printf("Default: warn\n");
}

/*=========================================================================
YYERROR
=========================================================================*/
int yyerror ( char *s ) /* Called by yyparse on error */
{
  return 0;
}

void funError(int errorCode, char * name){
  int line = numLineStack[include_stack_ptr];
  switch(errorCode) {
    case FUN_NOT_EXIST:
      print_error("ERROR on line %d function %s not exists\n", line, name);
       break;
    case LOC_VAR_ALREADY_EXIST:
      print_error("ERROR on line %d local var %s already exists\n", line, name);
       break;
    case LOC_VAR_NOT_EXIST:
      print_error("ERROR on line %d local var %s not exists\n", line, name);
       break;
    case FUN_ALREADY_EXIST:
      print_error("ERROR on line %d function %s already exists\n", line, name);
       break;
    default:
       break;
  }
}
/**************************** End Grammar File ***************************/
