/*************************************************************************** 
  Logger
***************************************************************************/ 

#ifndef __LOGGER__
#define __LOGGER__

/* Color definitions */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"

/* Verbosity levels */
#define INFO 2
#define WARN 1
#define ERROR 0

void print_begin_info();
void print_info(const char *format, ...);
void print_warn(const char *format, ...);
void print_error(const char *format, ...);
void print_final_info();

#endif