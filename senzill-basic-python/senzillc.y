%{
/************************************************************************* 
Compiler for the Simple language (with basic python bytecode compilation)
Author: Anthony A. Aaby
Modified by: Jordi Planes
Extended by: Joel Cemeli
             Eduard Torres
***************************************************************************/ 
/*========================================================================= 
C Libraries, Symbol Table, Code Generator & other C code 
=========================================================================*/ 
#include <stdio.h> /* For I/O */ 
#include <stdlib.h> /* For malloc here and in symbol table */ 
#include <string.h> /* For strcmp in symbol table */ 
#include <unistd.h> /* For argument parsing */
#include "ST.h" /* Symbol Table */ 
#include "SM.h" /* Stack Machine */ 
#include "CGSwitch.h" /* Code Generator Switch */ 
#include "logger.h" /* Loggin custom file */

#define YYDEBUG 1 /* For Debugging */ 

int yyerror(char *);
int yylex();
extern FILE *yyin;
extern int verbosity;
extern int n_errors;
extern int  currentLine;
char* mainFileName;

void funError(int errorCode, char * name);
void print_help();

int errors; /* Error Count */ 
/*------------------------------------------------------------------------- 
The following support backpatching 
-------------------------------------------------------------------------*/ 
struct lbs /* Labels for data, if and while */ 
{ 
  int for_goto; 
  int for_jmp_false; 
}; 

struct lbs * newlblrec() /* Allocate space for the labels */ 
{ 
   return (struct lbs *) malloc(sizeof(struct lbs)); 
}

/*------------------------------------------------------------------------- 
Install identifier & check if previously defined. 
-------------------------------------------------------------------------*/ 
void install ( char *sym_name ) 
{ 
  symrec *s = getsym (sym_name); 
  if (s == 0) {
    gen_name(sym_name);
    s = putsym (sym_name);
  } else { 
    char message[ 100 ];
    sprintf( message, "%s is already defined\n", sym_name ); 
    yyerror( message );
  } 
} 

/*------------------------------------------------------------------------- 
If identifier is defined, generate code 
-------------------------------------------------------------------------*/ 
int context_check( char *sym_name ) 
{ 
  symrec *identifier = getsym( sym_name ); 
  return identifier->offset;
} 

/*========================================================================= 
SEMANTIC RECORDS 
=========================================================================*/ 
%} 

%union /* semrec - The Semantic Records */ 
 { 
   int intval; /* Integer values */ 
   char *id; /* Identifiers */ 
   struct lbs *lbls; /* For backpatching */ 
};

/*========================================================================= 
TOKENS 
=========================================================================*/ 
%start program 
%token <intval> NUMBER /* Simple integer */ 
%token <id> IDENTIFIER /* Simple identifier */ 
%token <lbls> IF WHILE /* For backpatching labels */ 
%token SKIP THEN ELSE FI DO END 
%token INTEGER READ WRITE LET IN 
%token ASSGNOP 

/*========================================================================= 
OPERATOR PRECEDENCE 
=========================================================================*/ 
%left '-' '+' 
%left '*' '/' 
%right '^' 

/*========================================================================= 
GRAMMAR RULES for the Simple language 
=========================================================================*/ 

%% 

program : LET declarations IN { 
                                if (is_sz_compilation())
                                  gen_code( DATA, data_location() - 1 );
                              } 
          commands END { gen_code( HALT, 0 ); YYACCEPT; } 
; 

declarations : /* empty */ 
    | INTEGER id_seq IDENTIFIER '.' { install( $3 ); print_info("Added variable %s", $3); } 
    | error '.' { print_error("Bad declaration"); yyerrok; }
; 

id_seq : /* empty */ 
    | id_seq IDENTIFIER ',' { install( $2 ); print_info("Added variable %s", $2); } 
; 

commands : /* empty */ 
    | commands command ';'
    | error '.' { print_error("Unrecognized command"); yyerrok; }
; 

command : SKIP 
   | READ IDENTIFIER { gen_code( READ_INT, context_check( $2 ) ); } 
   | WRITE exp { gen_code( WRITE_INT, 0 ); } 
   | IDENTIFIER ASSGNOP exp { gen_code( STORE, context_check( $1 ) ); } 
   | IF bool_exp { $1 = (struct lbs *) newlblrec(); $1->for_jmp_false = reserve_loc(); } 
   THEN commands { $1->for_goto = reserve_loc(); } ELSE { 
     back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() ); 
   } commands FI { back_patch( $1->for_goto, GOTO, gen_label() ); } 
   | WHILE { $1 = (struct lbs *) newlblrec(); $1->for_goto = gen_label(); } 
   bool_exp { $1->for_jmp_false = reserve_loc(); } DO commands END { gen_code( GOTO, $1->for_goto ); 
   back_patch( $1->for_jmp_false, JMP_FALSE, gen_label() ); } 
;

bool_exp : exp '<' exp { gen_code( LT, 0 ); } 
   | exp '=' exp { gen_code( EQ, 0 ); } 
   | exp '>' exp { gen_code( GT, 0 ); } 
;

exp : NUMBER { gen_code( LD_INT, $1 ); } 
   | IDENTIFIER { gen_code( LD_VAR, context_check( $1 ) ); } 
   | exp '+' exp { gen_code( ADD, 0 ); } 
   | exp '-' exp { gen_code( SUB, 0 ); } 
   | exp '*' exp { gen_code( MULT, 0 ); } 
   | exp '/' exp { gen_code( DIV, 0 ); } 
   | exp '^' exp { gen_code( PWR, 0 ); } 
   | '(' exp ')' 
;

%% 

extern struct instruction code[ MAX_MEMORY ];

/*=========================================================================
MAIN
=========================================================================*/
int main( int argc, char *argv[] )
{
  int option = 0;
  char * in_file = NULL;
  char * out_file = NULL;

  verbosity = WARN;
  compile_to_sz();
  /* Argument parsing */
  while ((option = getopt(argc, argv, "i:o:v:p::")) != -1) {
    switch (option) {
      case 'i':
        in_file = strdup(optarg);
        break;
      case 'o':
        out_file = strdup(optarg);
        break;
      case 'v':
        if (strcasecmp(optarg, "info") == 0) {
          verbosity = INFO;
        } else if (strcasecmp(optarg, "warn") == 0) {
          verbosity = WARN;
        } else if (strcasecmp(optarg, "error") == 0) {
          verbosity = ERROR;
        } else {
          print_error("Unreconized option: %s", optarg);
          print_help();
          return -1;
        }
        break;
      case 'p':
        print_info("Python compilation activated");
        compile_to_py();
        break;
      default:
        print_help();
        exit(-1);
    }
  }
  if (in_file == NULL) {
    print_error("Input file is mandatory");
    print_help();
    exit(-1);
  }
  if (out_file == NULL) {
    print_error("Output file is mandatory");
    print_help();
    exit(-1);
  }
  /* Put main file to include symbol tab.
     Only works if this name is equals to the name in equals */
  mainFileName = strdup(in_file);
  yyin = fopen( in_file, "r" );
  if (yyin == NULL) {
    print_error("File %s not found", in_file);
    return -2;
  }
  /*yydebug = 1;*/
  print_begin_info();
  yyparse ();
  print_final_info();
  if ( n_errors == 0 )
    {
      //print_code ();
      //fetch_execute_cycle();
      write_bytecode( out_file );
    }
  return 0;
}

void print_help() {
  printf("usage: -i <input-file> -o <output-file> [-v <verb-level>] [-p]\n\n");
  printf("> -i: input file\n\n");
  printf("> -o: output file\n\n");
  printf("> -v: verbosity level\n");
  printf("> verb-level:\n");
  printf(" - info\n");
  printf(" - warn\n");
  printf(" - error\n");
  printf("> Default: warn\n\n");
  printf("> -p: compile to python bytecode\n");
}

/*=========================================================================
YYERROR
=========================================================================*/
int yyerror ( char *s ) /* Called by yyparse on error */
{
  return 0;
}

/**************************** End Grammar File ***************************/


